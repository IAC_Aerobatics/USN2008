<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet creates index of articles with an abstract, image, and link
for each article.  Supports markup of the form documented in "articleList.css."

Result conforms RSS 2.0 markup.
-->
<xsl:stylesheet version='1.0' 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" indent="yes"/>

<xsl:param name="file-sep">/</xsl:param>

<xsl:template match="articleList[child::rssfeed]">
      <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
         <channel>
           <atom:link rel="self" type="application/rss+xml">
           <xsl:attribute name="href"><xsl:value-of select="rssfeed/@rsslink"/></xsl:attribute>
           </atom:link>
           <title>
             <xsl:value-of select="rssfeed/@title"/>
           </title>
           <language>en-us</language>
           <link>
             <xsl:value-of select="rssfeed/@rsslink"/>
           </link>
           <description>
             <xsl:value-of select="rssfeed/@description"/>
           </description>
           <xsl:apply-templates select="article"/>
         </channel>
      </rss>
</xsl:template>

<xsl:template match="article">
  <item>
       <title>
         <xsl:value-of select="@title"/>
       </title>
       <description>
         <xsl:apply-templates select="abstract"/>
       </description>
       <xsl:if test="@link">
         <link>
           <xsl:value-of select="@link"/>
         </link>
       </xsl:if>
       <xsl:if test="@date">
         <pubDate>
           <xsl:value-of select="@date"/>
         </pubDate>
       </xsl:if>
       <guid>
         <xsl:value-of select="preceding-sibling::rssfeed/@rsslink"/>
         <xsl:text>/item</xsl:text>
         <xsl:value-of select="count(following-sibling::article) + 1"/>
       </guid>
  </item>
</xsl:template>

<xsl:template match="abstract">
  <xsl:value-of select="text()"/>
  <xsl:apply-templates mode="text"/>
</xsl:template>

<xsl:template match="link" mode="text">
  <xsl:value-of select="@ref"/>
</xsl:template>

<xsl:template match="node()" mode="text">
  <xsl:value-of select="text()"/>
  <xsl:apply-templates mode="text"/>
</xsl:template>        

<xsl:template match="text()"/>

</xsl:stylesheet>
