cat overall.html | sed -e '1,$s/{category}/Primary/g' >Primary.htm
cat flight.html | sed -e '1,$s/{category}/Primary/g' |
 sed -e '1,$s/{flight}/Flight I/g' |
 sed -e '1,$s/{schedule}/Tuesday, September 23/g' >PrimaryFlight1.htm
cat flight.html | sed -e '1,$s/{category}/Primary/g' |
 sed -e '1,$s/{flight}/Flight II/g' |
 sed -e '1,$s/{schedule}/Wednesday, September 24/g' >PrimaryFlight2.htm
cat flight.html | sed -e '1,$s/{category}/Primary/g' |
 sed -e '1,$s/{flight}/Flight III/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >PrimaryFlight3.htm
cat overall.html | sed -e '1,$s/{category}/Sportsman/g' >Sportsman.htm
cat flight.html | sed -e '1,$s/{category}/Sportsman/g' |
 sed -e '1,$s/{flight}/Flight I/g' |
 sed -e '1,$s/{schedule}/Tuesday, September 23/g' >SportsmanFlight1.htm
cat flight.html | sed -e '1,$s/{category}/Sportsman/g' |
 sed -e '1,$s/{flight}/Flight II/g' |
 sed -e '1,$s/{schedule}/Wednesday, September 24/g' >SportsmanFlight2.htm
cat flight.html | sed -e '1,$s/{category}/Sportsman/g' |
 sed -e '1,$s/{flight}/Flight III/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >SportsmanFlight3.htm
cat overall.html | sed -e '1,$s/{category}/Intermediate/g' >Intermediate.htm
cat flight.html | sed -e '1,$s/{category}/Intermediate/g' |
 sed -e '1,$s/{flight}/Known/g' |
 sed -e '1,$s/{schedule}/Monday, September 22/g' >IntermediateKnown.htm
cat flight.html | sed -e '1,$s/{category}/Intermediate/g' |
 sed -e '1,$s/{flight}/Free/g' |
 sed -e '1,$s/{schedule}/Wednesday, September 24/g' >IntermediateFree.htm
cat flight.html | sed -e '1,$s/{category}/Intermediate/g' |
 sed -e '1,$s/{flight}/Unknown/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >IntermediateUnknown.htm
cat overall.html | sed -e '1,$s/{category}/Advanced/g' >Advanced.htm
cat flight.html | sed -e '1,$s/{category}/Advanced/g' |
 sed -e '1,$s/{flight}/Known/g' |
 sed -e '1,$s/{schedule}/Monday, September 22/g' >AdvancedKnown.htm
cat flight.html | sed -e '1,$s/{category}/Advanced/g' |
 sed -e '1,$s/{flight}/Free/g' |
 sed -e '1,$s/{schedule}/Tuesday, September 23/g' >AdvancedFree.htm
cat flight.html | sed -e '1,$s/{category}/Advanced/g' |
 sed -e '1,$s/{flight}/Unknown/g' |
 sed -e '1,$s/{schedule}/Wednesday, September 24/g' >AdvancedUnknown.htm
cat overall.html | sed -e '1,$s/{category}/Unlimited/g' >Unlimited.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Known/g' |
 sed -e '1,$s/{schedule}/Sunday, September 21/g' >UnlimitedKnown.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Free/g' |
 sed -e '1,$s/{schedule}/Monday, September 22/g' >UnlimitedFree.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/First Unknown/g' |
 sed -e '1,$s/{schedule}/Tuesday, September 23/g' >UnlimitedUnknown1.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Second Unknown/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >UnlimitedUnknown2.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Four Minute/g' |
 sed -e '1,$s/{schedule}/Friday, September 26/g' >UnlimitedFourMinute.htm
cat overall.html | sed -e '1,$s/{category}/Sportsman/g' >GliderSportsman.htm
cat flight.html | sed -e '1,$s/{category}/Sportsman/g' |
 sed -e '1,$s/{flight}/Flight I/g' |
 sed -e '1,$s/{schedule}/Tuesday, September 23/g' >GliderSportsmanFlight1.htm
cat flight.html | sed -e '1,$s/{category}/Sportsman/g' |
 sed -e '1,$s/{flight}/Flight II/g' |
 sed -e '1,$s/{schedule}/Wednesday, September 24/g' >GliderSportsmanFlight2.htm
cat flight.html | sed -e '1,$s/{category}/Sportsman/g' |
 sed -e '1,$s/{flight}/Flight III/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >GliderSportsmanFlight3.htm
cat overall.html | sed -e '1,$s/{category}/Intermediate/g' >GliderIntermediate.htm
cat flight.html | sed -e '1,$s/{category}/Intermediate/g' |
 sed -e '1,$s/{flight}/Known/g' |
 sed -e '1,$s/{schedule}/Monday, September 22/g' >GliderIntermediateKnown.htm
cat flight.html | sed -e '1,$s/{category}/Intermediate/g' |
 sed -e '1,$s/{flight}/Free/g' |
 sed -e '1,$s/{schedule}/Wednesday, September 24/g' >GliderIntermediateFree.htm
cat flight.html | sed -e '1,$s/{category}/Intermediate/g' |
 sed -e '1,$s/{flight}/Unknown/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >GliderIntermediateUnknown.htm
cat overall.html | sed -e '1,$s/{category}/Unlimited/g' >GliderUnlimited.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Known/g' |
 sed -e '1,$s/{schedule}/Sunday, September 21/g' >GliderUnlimitedKnown.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Free/g' |
 sed -e '1,$s/{schedule}/Monday, September 22/g' >GliderUnlimitedFree.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/First Unknown/g' |
 sed -e '1,$s/{schedule}/Tuesday, September 23/g' >GliderUnlimitedUnknown1.htm
cat flight.html | sed -e '1,$s/{category}/Unlimited/g' |
 sed -e '1,$s/{flight}/Second Unknown/g' |
 sed -e '1,$s/{schedule}/Thursday, September 25/g' >GliderUnlimitedUnknown2.htm
