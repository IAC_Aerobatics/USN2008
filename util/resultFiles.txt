Web Contest Results
Web page title: use copy
Folder for reports: as desired. Note: deletes and replaces all files
Web main report filename: as below, e.g. "Primary"
*** UNCHECK *** "Create Pilots FairPlay Processed Marks sheets

Web Results Upload Manager
Primary folder: location used for web contest results
Server URI: iacusn.org
Sub-folder: leave blank, unchecked
Username: usn2008@iacusn.org
Password: FF2eq6P6 

Primary.htm Primary Overall
PrimaryFlight1.htm Primary Flight I
PrimaryFlight2.htm Primary Flight II
PrimaryFlight3.htm Primary Flight III
Sportsman.htm Sportsman Overall
SportsmanFlight1.htm Sportsman Flight I
SportsmanFlight2.htm Sportsman Flight II
SportsmanFlight3.htm Sportsman Flight III
Intermediate.htm Intermediate Overall
IntermediateKnown.htm Intermediate Known
IntermediateFree.htm Intermediate Free
IntermediateUnknown.htm Intermediate Unknown
Advanced.htm Advanced Overall
AdvancedKnown.htm Advanced Known
AdvancedFree.htm Advanced Free
AdvancedUnknown.htm Advanced Unknown
Unlimited.htm Unlimited Overall
UnlimitedKnown.htm Unlimited Known
UnlimitedFree.htm Unlimited Free
UnlimitedUnknown1.htm Unlimited First Unknown
UnlimitedUnknown2.htm Unlimited Second Unknown
UnlimitedFourMinute.htm Unlimited Four Minute
GliderSportsman.htm Sportsman Overall
GliderSportsmanFlight1.htm Sportsman Flight I
GliderSportsmanFlight2.htm Sportsman Flight II
GliderSportsmanFlight3.htm Sportsman Flight III
GliderIntermediate.htm Intermediate Overall
GliderIntermediateKnown.htm Intermediate Known
GliderIntermediateFree.htm Intermediate Free
GliderIntermediateUnknown.htm Intermediate Unknown
GliderUnlimited.htm Unlimited Overall
GliderUnlimitedKnown.htm Unlimited Known
GliderUnlimitedFree.htm Unlimited Free
GliderUnlimitedUnknown1.htm Unlimited First Unknown
GliderUnlimitedUnknown2.htm Unlimited Second Unknown
